from distutils.command.upload import upload
from django.db import models
from django.urls import reverse
from category.models import Category


class Restraunt(models.Model):
  title = models.CharField(verbose_name='Название', max_length=255)
  category = models.ForeignKey(verbose_name='Категория', to=Category, on_delete=models.CASCADE)
  description = models.TextField(verbose_name='Описание', max_length=255)
  rate = models.FloatField(verbose_name='Рейтинг', max_length=255)
  cost_of_delivery = models.FloatField(verbose_name='Цена доставки', max_length=255)
  time_of_delivery = models.TimeField(verbose_name='Время доставки', max_length=255)
  cover = models.ImageField(verbose_name='Обложка ресторана', upload_to='restraunts_cards')
  banner = models.ImageField(verbose_name='Баннер ресторана', upload_to='restraunts_banners')


  def __str__(self):
    return self.title

  class Meta:
    verbose_name = 'Ресторан'
    verbose_name_plural = 'Рестораны'


