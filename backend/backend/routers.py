from rest_framework.routers import DefaultRouter
from restraunt.views import RestrauntViewSet
from category.views import CategoryViewSet
from dish.views import DishViewSet
from order.views import OrderViewSet
from review.views import ReviewViewSet

router = DefaultRouter()

router.register('restraunt', RestrauntViewSet)
router.register('category', CategoryViewSet)
router.register('dish', DishViewSet)
router.register('order', OrderViewSet)
router.register('review', ReviewViewSet)