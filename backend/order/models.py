from django.db import models
from dish.models import Dish
from restraunt.models import Restraunt
from authentication.models import User


class Order(models.Model):
  recipient = models.ForeignKey(verbose_name='Получатель', to=User, on_delete=models.CASCADE)
  comment = models.TextField(verbose_name='Комментарий к заказу', max_length=255)
  final_cost = models.FloatField(verbose_name='Сумма заказа', max_length=255)
  goods = models.ManyToManyField(verbose_name='Товары в заказе', to=Dish)
  restraunt = models.ForeignKey(verbose_name='Ресторан', to=Restraunt, on_delete=models.CASCADE)


  def __str__(self):
    return self.comment

  class Meta:
    verbose_name = 'Заказ'
    verbose_name_plural = 'Заказы'