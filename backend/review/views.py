from rest_framework.viewsets import ModelViewSet
from review.serializers import ReviewSerializer
from review.models import Review


class ReviewViewSet(ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
