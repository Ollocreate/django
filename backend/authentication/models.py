from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from authentication.managers import UserManager

class User(AbstractBaseUser, PermissionsMixin):
  email = models.CharField(verbose_name='Email', max_length=255, unique=True)
  name = models.CharField(verbose_name='Имя', max_length=255)
  phone_number = models.CharField(verbose_name='Телефон', max_length=255)
  address = models.CharField(verbose_name='Адрес', max_length=255)

  is_active = models.BooleanField(verbose_name='Активирован', default=False)
  is_staff = models.BooleanField(verbose_name='Модерация', default=False)
  is_superuser = models.BooleanField(verbose_name='Админ', default=False)

  USERNAME_FIELD = 'email'
  REQUIRED_FIELDS = ['name', 'phone_number', 'address']

  objects = UserManager()

  def __str__(self):
    return self.email

  class Meta:
    verbose_name = 'Пользователь'
    verbose_name_plural = 'Пользователи'