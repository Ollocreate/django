from rest_framework import serializers
from dish.models import Dish
from restraunt.models import Restraunt

class RestrauntSerializerForDish(serializers.ModelSerializer):
    class Meta:
        model = Restraunt
        fields = ['title', ]


class DishSerializer(serializers.ModelSerializer):
    restraunt_data = RestrauntSerializerForDish(source="restraunt")
    class Meta:
        model = Dish
        exclude = ['restraunt', ]

